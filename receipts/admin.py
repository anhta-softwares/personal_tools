# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Receipt
from .models import Purpose
from .models import SharePurpose

# from django.contrib.admin import DateFieldListFilter
from daterange_filter.filter import DateRangeFilter
from django.contrib.admin.views.main import ChangeList
from django.db.models import Count, Sum, Avg


class MyChangeList(ChangeList):
    def get_results(self, *args, **kwargs):
        super(MyChangeList, self).get_results(*args, **kwargs)
        q = self.result_list.aggregate(receipt_sum=Sum("amount"))
        self.receipt_count = q["receipt_sum"]


class ReceiptAdmin(admin.ModelAdmin):
    model = Receipt
    exclude = ["created_by"]
    list_display = [
        "name",
        "amount",
        "pay_date",
        "get_share_purposes",
        "get_purposes",
        "processed",
        "comment",
        "upload",
    ]
    list_editable = ["processed"]
    search_fields = ["name", "purposes__name"]
    list_filter = (
        ("pay_date", DateRangeFilter),
        ("purposes"),
        ("share_purposes"),
        ("share_with"),
    )

    def get_purposes(self, obj):
        return "; ".join([p.name for p in obj.purposes.all()])

    def get_share_purposes(self, obj):
        return "; ".join([p.name for p in obj.share_purposes.all()])

    def get_name(self, obj):
        return obj.receipt.name

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        qs = super(ReceiptAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)

    def get_changelist(self, request, **kwargs):
        return MyChangeList


class SharedReceipt(Receipt):
    class Meta:
        proxy = True


class SharedReceiptAdmin(admin.ModelAdmin):
    model = Receipt
    actions = None
    exclude = ["created_by", "share_with"]
    search_fields = ["name"]

    list_display = [
        "name",
        "amount",
        "pay_date",
        "get_share_purposes",
        "processed",
        "comment",
        "upload",
    ]
    list_filter = (("pay_date", DateRangeFilter), ("share_purposes"), ("processed"))

    list_editable = ["processed", "comment"]
    list_display_links = None

    def get_purposes(self, obj):
        return "; ".join([p.name for p in obj.purposes.all()])

    def get_share_purposes(self, obj):
        return "; ".join([p.name for p in obj.share_purposes.all()])

    def get_name(self, obj):
        return obj.receipt.name

    def save_model(self, request, obj, form, change):
        obj.save()

    def get_queryset(self, request):
        qs = super(SharedReceiptAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(share_with=request.user)

    get_purposes.short_description = "Category"  # Renames column head
    get_share_purposes.short_description = "Share purpose"  # Renames column head

    def get_changelist(self, request, **kwargs):
        return MyChangeList


admin.site.register(Receipt, ReceiptAdmin)
admin.site.register(SharedReceipt, SharedReceiptAdmin)
admin.site.register(Purpose)
admin.site.register(SharePurpose)

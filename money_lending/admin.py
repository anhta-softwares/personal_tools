# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.  from .models import Debtor
from .models import MoneyLending
from .models import Debtor

# from django.contrib.admin import DateFieldListFilter
from daterange_filter.filter import DateRangeFilter


class MoneyLendingAdmin(admin.ModelAdmin):
    model = MoneyLending
    exclude = ["created_by"]

    list_display = ["debtor", "purpose", "amount", "lend_date", "paid", "transaction"]
    search_fields = ["purpose", "debtor", "amount"]
    list_filter = (("lend_date", DateRangeFilter), ("debtor"))

    def get_name(self, obj):
        return obj.receipt.name

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    def get_queryset(self, request):
        qs = super(MoneyLendingAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)

    get_name.admin_order_field = "purpose"  # Allows column order sorting
    get_name.short_description = "Purpose"  # Renames column head


admin.site.register(MoneyLending, MoneyLendingAdmin)
admin.site.register(Debtor)

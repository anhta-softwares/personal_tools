# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class MoneyLendingConfig(AppConfig):
    name = "money_lending"
    verbose_name = "Money lending"

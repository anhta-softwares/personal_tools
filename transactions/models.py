# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django_extensions.db.models import TimeStampedModel
from audit_log.models.fields import CreatingUserField, CreatingSessionKeyField
from django.contrib import admin
from django.conf import settings


class TransactionFile(models.Model):
    upload_date = models.DateTimeField("Upload date")
    upload_file = models.FileField(upload_to="uploads/transactions/", null=True)
    created_by = models.ForeignKey(User, null=True)
    is_processed = models.BooleanField(default=False)

    def __unicode_(self):
        self.upload_file


class Transaction(models.Model):
    from_account = models.CharField(null=True, blank=True, max_length=50)
    entry_date = models.DateField("Entry date")
    value_date = models.DateField("Value date")
    payment_date = models.DateField("Payment date")
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    beneficiary = models.CharField(null=True, blank=True, max_length=50)
    to_account = models.CharField(null=True, blank=True, max_length=50)
    BIC = models.CharField(null=True, blank=True, max_length=50)
    transaction = models.CharField(null=True, blank=True, max_length=50)
    ref_num = models.CharField(null=True, blank=True, max_length=100)
    originator_ref = models.CharField(null=True, blank=True, max_length=200)
    message = models.CharField(null=True, blank=True, max_length=200)
    card_num = models.CharField(null=True, blank=True, max_length=100)
    receipt = models.CharField(null=True, blank=True, max_length=100)
    created_from_file = models.ForeignKey(TransactionFile)

    #    class Meta:
    #        unique_together = ["from_account", "entry_date", "value_date", "payment_date", "amount", "beneficiary", \
    #        "to_account", "BIC", "transaction", "ref_num", "originator_ref", "message", "card_num", "receipt"]

    def __unicode__(self):
        return self.ref_num

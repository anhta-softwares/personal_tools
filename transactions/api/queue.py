import pika
import sys
from django.conf import settings


def add(message, queue_name="five_kpi"):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost"))
    # credentials=pika.credentials.PlainCredentials(settings.RABBIT_MQ_LOGIN,
    #                                              settings.RABBIT_MQ_PASSWORD)))
    channel = connection.channel()
    # don't dispatch a new message to a worker until it has processed and acknowledged the previous one
    channel.basic_qos(prefetch_count=1)
    # durable True stores messages even if rabbitmq server shuts down
    channel.queue_declare(
        queue=queue_name, durable=True, arguments={"x-max-priority": 10}
    )
    channel.basic_publish(
        exchange="",
        routing_key=queue_name,
        body=message,
        properties=pika.BasicProperties(delivery_mode=2),
    )

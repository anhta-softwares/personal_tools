# Personal tools

- Author: Anh Ta
- Created: 05/2017

## List of supported apps
### Receipts app

- Store personal receipts info (upload receipt images, values, etc.)
- Share selected receipts with other users

### Transactions app

- Parse CSV files from bank (Nordea) and store them into the database
- Support basic filters (payer, beneficiary, amount, pay date, etc.)

### Money lending app

- Just for my personal use case, when some of the transactions were lended to someone, we can link from transaction app to this app

## Deployment / starting the server
- Build app
```sh
make build-app
```
- Run app
```sh
make run
```
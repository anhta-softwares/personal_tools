build-app:
	- docker-compose build

run:
	- echo "Deploying app ..."
	- docker-compose up -d
	- echo "The app is accessible via port :8000"

import json
import logging
import datetime
import sys
import os

# from transactions import models as t_models


class TransactionsParser(object):
    """Main parser for transaction reports"""

    def __init__(self, transactions_csv):
        # self.kpi_json = json.loads(kpi_json)
        self.transactions_csv = transactions_csv

    def process(self):
        """parse this json, save to db, etc.."""
        logging.info("Parsing transaction csv file")
        self.parse_csv()
        logging.info("Parsing transaction csv done")

        return "Parsing successful"

    def parse_csv(self):
        """
        TODO
        """
        logging.error(self.transactions_csv)
        logging.info(t_models.TransactionFile.all())
        logging.info("Function called")


if __name__ == "__main__":
    import sys
    import logging
    import os

    #    sys.path.append("/home/tuananhta/GIT/personal_tools/")
    #    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "personal_tools.settings")
    #    from django.conf import settings
    #    settings.configure(settings)

    from transactions import models as t_models

    if 1 < len(sys.argv) <= 2:
        with open(sys.argv[1]) as f:
            CSV_DATA = f.read()
            # LOG = logging.getLogger('consumers')
            PARSER = TransactionsParser(transactions_csv=CSV_DATA)
            PARSER.process()

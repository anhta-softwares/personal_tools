import logging

# Log everything to the logs directory at the top
LOGFILE_ROOT = join("/var", "logs", "perosnal_tools")

# Reset logging
LOGGING_CONFIG = None
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "[%(asctime)s] %(levelname)s [%(pathname)s:%(lineno)s] %(message)s",
            "datefmt": "%d/%b/%Y %H:%M:%S",
        },
        "medium": {
            "format": "[%(asctime)s] %(message)s",
            "datefmt": "%d/%b/%Y %H:%M:%S",
        },
        "simple": {"format": "%(levelname)s %(message)s"},
        "raw": {"format": "%(message)s"},
    },
    "handlers": {
        "consumers_log_file": {
            "level": "INFO",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "when": "midnight",
            "interval": 1,
            "backupCount": 7,
            "filename": join(LOGFILE_ROOT, "consumers.log"),
            "formatter": "verbose",
        },
        "loggers": {
            "consumers": {"handlers": ["consumers_log_file"], "level": "DEBUG",},
        },
    },
}

logging.config.dictConfig(LOGGING)

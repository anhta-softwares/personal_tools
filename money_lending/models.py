# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from django_extensions.db.models import TimeStampedModel
from audit_log.models.fields import CreatingUserField, CreatingSessionKeyField
from django.contrib import admin
from django.conf import settings
from transactions.models import Transaction


class Debtor(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class MoneyLending(models.Model):
    debtor = models.ForeignKey(Debtor)
    purpose = models.CharField(max_length=200)
    lend_date = models.DateTimeField("Lended date")
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    paid = models.BooleanField(default=False)
    # transaction_file = models.FileField(null=True, upload_to='uploads/%Y/%m/%d/')
    transaction = models.ForeignKey(Transaction, null=True)
    created_by = models.ForeignKey(User, related_name="%(class)s_created", null=True)

    def __unicode__(self):
        return self.purpose

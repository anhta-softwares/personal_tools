# Create your tasks here
from __future__ import absolute_import, unicode_literals
from personal_tools.celery import app
from transactions.models import TransactionFile, Transaction
import time
from django.core.exceptions import ObjectDoesNotExist
import os
import errno
from io import open
import datetime


@app.task
def analyze_file(file_id):
    try:
        file = TransactionFile.objects.get(pk=file_id)
        if not file.is_processed:
            transactions = Tool().read_file(file.upload_file.path)
            return Tool().import_transactions(transactions, file)
        else:
            return "already processed"
    except ObjectDoesNotExist:
        return "file does not exist"


class Tool:
    def __init__(self):
        pass

    @staticmethod
    def read_file(file_path):
        try:
            return open(file_path, "r", encoding="utf-8").readlines()
        except IOError as e:
            print(os.strerror(e.errno))
        return "Error"

    @staticmethod
    def import_transactions(transactions, file):
        for transaction in transactions:
            cols = transaction.split("\t")
            if cols[0] == "Account number":
                account_number = cols[1]
            if (
                cols[0].lstrip() != ""
                and cols[0] != "Account number"
                and cols[0] != "Entry date"
            ):
                tran_obj = Transaction(
                    from_account=account_number,
                    entry_date=datetime.datetime.strptime(cols[0], "%d.%m.%Y").date(),
                    value_date=datetime.datetime.strptime(cols[1], "%d.%m.%Y").date(),
                    payment_date=datetime.datetime.strptime(cols[2], "%d.%m.%Y").date(),
                    amount=float(cols[3].replace(",", ".")),
                    beneficiary=cols[4],
                    to_account=cols[5],
                    BIC=cols[6],
                    transaction=cols[7],
                    ref_num=cols[8],
                    originator_ref=cols[9],
                    message=cols[10],
                    card_num=cols[11],
                    receipt=cols[12],
                    created_from_file=file,
                )
                tran_obj.save()
        return str(len(transactions)) + " transactions have been created"

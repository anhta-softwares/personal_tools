# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import time
from datetime import datetime
from django.contrib import admin
from transactions.models import TransactionFile, Transaction
from transactions.tasks import analyze_file
from django.utils.translation import ugettext_lazy as _


class MoneyFilter(admin.SimpleListFilter):
    title = "Money filter"
    parameter_name = "money_filter"

    def lookups(self, request, model_admin):
        return (
            ("received", _("Money received")),
            ("spent", _("Money spent")),
        )

    def queryset(self, request, queryset):
        if self.value() == "received":
            return queryset.filter(amount__gte=0)

        if self.value() == "spent":
            return queryset.filter(amount__lt=0)


class TransactionFileAdmin(admin.ModelAdmin):
    model = TransactionFile
    exclude = ["created_by", "upload_date"]
    list_display = ["upload_date", "upload_file", "created_by"]

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.upload_date = datetime.now()
        obj.save()

        analyze_file.delay(obj.id)

    def get_queryset(self, request):
        qs = super(TransactionFileAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(created_by=request.user)


class TransactionAdmin(admin.ModelAdmin):
    model = Transaction
    exclude = ["is_processed"]
    list_display = [
        "from_account",
        "payment_date",
        "amount",
        "beneficiary",
        "transaction",
        "message",
    ]
    list_filter = (
        MoneyFilter,
        ("payment_date"),
    )

    def get_queryset(self, request):
        qs = super(TransactionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(created_from_file__created_by=request.user).order_by(
            "-payment_date"
        )


admin.site.register(TransactionFile, TransactionFileAdmin)
admin.site.register(Transaction, TransactionAdmin)

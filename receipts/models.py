# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from django_extensions.db.models import TimeStampedModel
from audit_log.models.fields import CreatingUserField, CreatingSessionKeyField
from django.contrib import admin
from django.conf import settings

# django.contrib.auth.models.User

# Create your models here.


class Purpose(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class SharePurpose(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name


class Receipt(models.Model):
    share_with = models.ManyToManyField(User, blank=True)
    share_purposes = models.ManyToManyField(SharePurpose, blank=True)
    purposes = models.ManyToManyField(Purpose, blank=True)
    name = models.CharField(max_length=200)
    pay_date = models.DateTimeField("date paid")
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    processed = models.BooleanField(default=False)
    upload = models.FileField(null=True, upload_to="uploads/%Y/%m/%d/")
    created_by = models.ForeignKey(User, related_name="%(class)s_created", null=True)
    comment = models.CharField(max_length=200, null=True, blank=True)

    def __unicode__(self):
        return self.name

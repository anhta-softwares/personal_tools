# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-19 19:21
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0007_auto_20170515_2019"),
    ]

    operations = [
        migrations.CreateModel(
            name="ShareReceipt",
            fields=[],
            options={"proxy": True, "indexes": [],},
            bases=("receipts.receipt",),
        ),
    ]
